export class AlertModel {
  public message: string;
  public type: string;
  public title?: string;
  public expires?: boolean = true;
}
