import { ConteudoModel } from './conteudo';
import { ImagemModel } from './imagem';
import { SeoModel } from './seo';

interface CursoBase {
  ID_DETALHE_CURSO?: Number;
  NOME?: string;
  SLUG?: string;
  CONTEUDO_CADASTRADO?: boolean;
}

export interface CursoModel extends CursoBase {
  CONTEUDO?: ConteudoModel;
  SEO?: SeoModel;
  IMAGENS?: CursoListaImagensModel;
}

export interface CursoListaModel extends CursoBase {
  FL_PUBLICAR: boolean;
}

export interface CursoListaImagensModel {
  DESTAQUE?: ImagemModel;
}
