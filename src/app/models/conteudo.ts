export interface ConteudoModel {
  ID_CONTEUDO?: Number;
  TEXTO?: string;
  FL_CONTEUDO: string;
  FL_PUBLICAR: boolean;
  DT_ATUALIZACAO_CONTEUDO?: Date;
}
