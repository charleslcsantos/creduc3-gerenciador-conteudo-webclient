import { ConteudoModel } from './conteudo';
import { ImagemModel } from './imagem';
import { SeoModel } from './seo';


interface InstituicaoBase {
  COD_INSTITUICAO?: string;
  NOME_FANTASIA?: string;
  CIDADE?: string;
  UF?: string;
  SLUG?: string;
  APELIDO?: string;
}

export interface InstituicaoModel extends InstituicaoBase {
  ENDERECO?: string;
  CEP?: string;
  DDD?: string;
  NUMERO?: string;

  SITE?: string;
  ORIGEM?: string;
  CONTEUDO?: ConteudoModel;
  SEO?: SeoModel;
  IMAGENS?: InstituicaoImagensModel;
}

export interface InstituicaoListaModel extends InstituicaoBase {
  EMAIL?: string;
  CONTEUDO_CADASTRADO: boolean;
  FL_PUBLICAR: boolean;
  DT_ULTIMA_ALTERACAO?: Date;
  DT_ATUALIZACAO_CONTEUDO?: Date;
}

export interface InstituicaoConteudoModel {
  COD_INSTITUICAO?: string;
  APELIDO?: string;
  SLUG?: string;
  CONTEUDO?: ConteudoModel;
}

export interface InstituicaoImagensModel {
  LOGO?: ImagemModel;
  BANNER_MOBILE?: ImagemModel;
  BANNER_DESKTOP?: ImagemModel;
  FACHADA?: ImagemModel;
}
