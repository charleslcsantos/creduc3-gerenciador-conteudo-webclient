
export enum TipoImagem {
  LOGO = 'L',
  BANNER = 'B',
  FACHADA = 'F'
}

export enum TamanhoImagem {
  MOBILE = 'SM',
  DESKTOP = 'LG'
}

export interface ArquivoModel {
  ID_ARQUIVO?: Number;
  NOME?: string;
  EXTENSAO?: string;
  DADOS?: string;
}

export interface ImagemModel {
  ID_IMAGEM?: Number;
  TAMANHO: TamanhoImagem;
  FL_TIPO?: string;
  ARQUIVO: ArquivoModel;
}
