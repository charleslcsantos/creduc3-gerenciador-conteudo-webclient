export interface SeoModel {
  ID_SEO?: Number;
  ID_CONTEUDO?: Number;
  TITLE?: string;
  DESCRIPTION?: string;
  OG_DESCRIPTION?: string;
  ROBOTS_INDEX?: boolean;
  ROBOTS_FOLLOW?: boolean;
  KEYWORDS?: string;
}
