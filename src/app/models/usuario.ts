export interface UsuarioModel {
  ID_USUARIO?: Number;
  NOME_COMPLETO?: string;
  NOME_USUARIO?: string;
  PRIMEIRO_NOME?: string;
  GRUPOS?: string[];
}
