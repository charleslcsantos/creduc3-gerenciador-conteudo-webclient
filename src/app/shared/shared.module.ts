import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router';

import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { OrigemComponent } from './origem/origem.component';
import { CursoComponent } from './curso/curso.component';

@NgModule({
  imports: [CommonModule],
  declarations: [
    HeaderComponent,
    HomeComponent,
    SidebarComponent,
    OrigemComponent,
    CursoComponent
  ]
})
class SharedModule {}
