import { AlertService } from './../../../services/alert/alert.service';
import { ArquivoModel } from './../../../models/imagem';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FileUploader, FileItem } from 'ng2-file-upload';

import { InstituicaoService } from '../../../services/instituicao.service';
import {
  InstituicaoModel,
  InstituicaoConteudoModel,
  InstituicaoImagensModel
} from './../../../models/instituicao';
import { TamanhoImagem, ImagemModel } from '../../../models/imagem';
import { SeoModel } from '../../../models/seo';
import { LoaderService } from '../../../services/loader/loader.service';

import 'rxjs/add/operator/finally';
import { FileSaverService } from '../../../services/filesaver.service';
import { UtilService } from '../../../services/utils/utils.service';

// import MediumEditorMultiPlaceholders from '../../../../../node_modules/medium-editor-multi-placeholders-plugin/src/main';

@Component({
  selector: 'app-instituicao-form',
  templateUrl: './instituicao-form.component.html',
  styleUrls: ['./instituicao-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InstituicaoFormComponent implements OnInit {
  instituicao: InstituicaoModel = {};
  seo: SeoModel = {};
  conteudo: string = '';
  apelido: string = '';
  slug: string = '';

  enabledFileSaver: Boolean = false;

  uploaderLogo: FileUploader;
  uploaderBannerMobile: FileUploader;
  uploaderBannerDesktop: FileUploader;
  uploaderImagemFachada: FileUploader;

  editorOptions = {};

  selectedTab = 'conteudo';

  constructor(
    private instituicaoService: InstituicaoService,
    private fileSaver: FileSaverService,
    private route: ActivatedRoute,
    private alertService: AlertService,
    private loaderService: LoaderService,
    private utilService: UtilService
  ) {
    this.loaderService.showLoader();
    this.editorOptions = {
      toolbar: {
        buttons: [
          'h2',
          'h3',
          'h4',
          'h5',
          'bold',
          'italic',
          'justifyFull',
          'anchor',
          'image',
          'orderedlist',
          'unorderedlist'
        ]
      },
      anchor: {
        placeholderText: 'Cole ou digite a url',
        targetCheckbox: true,
        targetCheckboxText: 'Abrir em nova janela'
      }
    };
  }

  ngOnInit() {
    this.route.params.subscribe(params => this.getInstituicao(params.id));

    this.enabledFileSaver = this.fileSaver.browserSupports();

    this.onAfterAddingFile(this.uploaderLogo);
    this.onAfterAddingFile(this.uploaderBannerMobile);
    this.onAfterAddingFile(this.uploaderBannerDesktop);
    this.onAfterAddingFile(this.uploaderImagemFachada);
  }

  slugify(){
    if (this.slug.length > 0) {
      this.utilService.slugify(this.slug).subscribe(slug => this.slug = slug);
    }
  }

  getInstituicao(id) {
    this.instituicao = {};
    this.uploaderLogo = new FileUploader({ url: 'https://url-da-api' });
    this.uploaderBannerMobile = new FileUploader({ url: 'https://url-da-api' });
    this.uploaderBannerDesktop = new FileUploader({
      url: 'https://url-da-api'
    });
    this.uploaderImagemFachada = new FileUploader({
      url: 'https://url-da-api'
    });

    this.instituicaoService.get(id).subscribe(data => {
      this.instituicao = data;
      this.slug = this.instituicao.SLUG;
      this.seo = this.instituicao.SEO;
      this.apelido = this.instituicao.APELIDO;

      try {
        this.conteudo = this.instituicao.CONTEUDO.TEXTO || '';
      } catch (error) {
        this.conteudo = '';
      }

      this.getImage(this.instituicao.IMAGENS.LOGO);
      this.getImage(this.instituicao.IMAGENS.BANNER_DESKTOP);
      this.getImage(this.instituicao.IMAGENS.BANNER_MOBILE);
      this.getImage(this.instituicao.IMAGENS.FACHADA);
      this.loaderService.showLoader(false);
    });
  }

  onAfterAddingFile(u: FileUploader) {
    u.onAfterAddingFile = f => {
      if (u.queue.length > 1) {
        u.removeFromQueue(u.queue[0]);
      }
    };
  }

  hasImageUploader(img): boolean {
    return (
      img &&
      'queue' in img &&
      img.queue &&
      img.queue.length > 0 &&
      img.queue[0]._file
    );
  }

  getImageUploader(img) {
    if (
      img &&
      'queue' in img &&
      img.queue &&
      img.queue.length > 0 &&
      img.queue[0]._file
    ) {
      return img.queue[0]._file;
    }
    return '';
  }

  getImage(destino: ImagemModel) {
    if (!destino) {
      return;
    }

    if (!('ARQUIVO' in destino) || destino.ARQUIVO == null) {
      destino.ARQUIVO = {};
    }

    destino.ARQUIVO.DADOS = '/assets/images/image-wireframe.png';
    this.instituicaoService
      .getImagem(destino.ARQUIVO.ID_ARQUIVO)
      .subscribe(arquivo => {
        if (arquivo) {
          destino.ARQUIVO.DADOS = arquivo;
        }
      });
  }
  renderImage(key: string): string {
    key = key.toUpperCase();

    try {
      if (this.instituicao.IMAGENS[key].ARQUIVO.DADOS) {
        return this.instituicao.IMAGENS[key].ARQUIVO.DADOS;
      }
    } catch (err) {
      return '/assets/images/image-wireframe.png';
    }
  }

  publish(): void {
    this.instituicaoService
    .publish(this.instituicao.COD_INSTITUICAO)
    .finally(() => this.loaderService.showLoader(false))
    .subscribe(
      data => {
        this.alertService.success('O site foi publicado');
        this.instituicao.CONTEUDO.FL_PUBLICAR = true;
      },
      error => {
        this.alertService.error(
          'Ocorreu um erro ao tentar publicar o site'
        );
      }
    );
  }

  downloadDoc(): void {
    this.fileSaver.downloadFile(this.instituicao.SLUG, this.instituicao.CONTEUDO.TEXTO, this.instituicao.CONTEUDO.DT_ATUALIZACAO_CONTEUDO);
  }

  saveDetail(): void {
    this.loaderService.showLoader();

    const model: InstituicaoConteudoModel = {};
    model.COD_INSTITUICAO = this.instituicao.COD_INSTITUICAO;
    model.SLUG = this.slug;
    model.APELIDO = this.apelido;
    model.CONTEUDO = this.instituicao.CONTEUDO;

    this.instituicaoService
      .save(this.instituicao.COD_INSTITUICAO, model)
      .finally(() => this.loaderService.showLoader(false))
      .subscribe(
        data => {
          this.instituicao.SLUG = data.SLUG;
          this.slug = data.SLUG;
          this.alertService.success('As alterações foram salvas!');
        },
        error => {
          this.alertService.error(
            'Ocorreu um erro ao tentar editar os dados da instituição'
          );
        }
      );
  }

  saveLogo(): void {
    this.loaderService.showLoader();
    const form = new FormData();
    const logo: FileItem = this.uploaderLogo.queue[0];

    if (logo) {
      form.append('logo', logo._file, logo.file.name);
    }

    this.instituicaoService
      .saveImages(this.instituicao.COD_INSTITUICAO, form)
      .finally(() => this.loaderService.showLoader(false))
      .subscribe(
        data => {
          console.log(data);
          this.alertService.success(
            'Pronto! A logo da instituição foi alterada!'
          );
        },
        error => {
          this.alertService.error(
            'Ops. Ocorreu um erro ao tentar editar a logo da instituição!'
          );
        }
      );
  }

  saveConteudo(): void {
    this.loaderService.showLoader();
    const instituicaoModel: InstituicaoConteudoModel = {};
    instituicaoModel.COD_INSTITUICAO = this.instituicao.COD_INSTITUICAO;
    instituicaoModel.CONTEUDO = this.instituicao.CONTEUDO;
    instituicaoModel.CONTEUDO.TEXTO = this.conteudo;

    this.instituicaoService
      .save(this.instituicao.COD_INSTITUICAO, instituicaoModel)
      .finally(() => this.loaderService.showLoader(false))
      .subscribe(
        data => {
          this.alertService.success('O Conteúdo foi salvo!');
          this.instituicao.CONTEUDO = data.CONTEUDO;
        },
        error => {
          console.warn(error);
          this.alertService.error(
            'Ops. Ocorreu um erro ao tentar salvar o conteúdo!'
          );
        }
      );
  }

  saveImages(): void {
    this.loaderService.showLoader();
    const bannerDesktop: FileItem = this.uploaderBannerDesktop.queue[0];
    const bannerMobile: FileItem = this.uploaderBannerMobile.queue[0];
    const imagemFachada: FileItem = this.uploaderImagemFachada.queue[0];

    const form = new FormData();

    if (bannerDesktop) {
      form.append(
        'banner_desktop',
        bannerDesktop._file,
        bannerDesktop.file.name
      );
    }
    if (bannerMobile) {
      form.append('banner_mobile', bannerMobile._file, bannerMobile.file.name);
    }

    if (imagemFachada) {
      form.append('fachada', imagemFachada._file, imagemFachada.file.name);
    }

    this.instituicaoService
      .saveImages(this.instituicao.COD_INSTITUICAO, form)
      .finally(() => this.loaderService.showLoader(false))
      .subscribe(
        data => {
          this.alertService.success('As imagens foram atualizadas!');
        },
        error => {
          this.alertService.error(
            'Ops. Ocorreu um erro ao tentar atualizar as imagens.'
          );
        }
      );
  }

  saveSeo(): void {
    this.loaderService.showLoader();
    let seoModel: SeoModel = {};
    seoModel = this.seo;

    this.instituicaoService
      .saveSeo(this.instituicao.CONTEUDO.ID_CONTEUDO, seoModel)
      .finally(() => this.loaderService.showLoader(false))
      .subscribe(
        data => {
          this.seo = data;
          this.alertService.success('As configurações de SEO foram salvas!');
        },
        error => {
          this.alertService.error(
            'Ops. Ocorreu um erro ao tentar salvar o conteúdo de SEO!'
          );
        }
      );
  }

  submit() {
    this.loaderService.showLoader();
    const instituicaoModel: InstituicaoConteudoModel = {};
    instituicaoModel.COD_INSTITUICAO = this.instituicao.COD_INSTITUICAO;
    instituicaoModel.SLUG = this.slug;
    instituicaoModel.CONTEUDO = this.instituicao.CONTEUDO;
    instituicaoModel.CONTEUDO.TEXTO = this.conteudo;

    const logo: FileItem = this.uploaderLogo.queue[0];
    const bannerDesktop: FileItem = this.uploaderBannerDesktop.queue[0];
    const bannerMobile: FileItem = this.uploaderBannerMobile.queue[0];

    const form = new FormData();

    if (logo) {
      form.append('logo', logo._file, logo.file.name);
    }
    if (bannerDesktop) {
      form.append(
        'banner_desktop',
        bannerDesktop._file,
        bannerDesktop.file.name
      );
    }
    if (bannerMobile) {
      form.append('banner_mobile', bannerMobile._file, bannerMobile.file.name);
    }

    const params = {
      formInstituicao: instituicaoModel,
      formImagens: form
    };

    this.instituicaoService
      .saveAll(this.instituicao.COD_INSTITUICAO, params)
      .finally(() => this.loaderService.showLoader(false))
      .subscribe(
        data => {
          console.log(data);
          this.instituicao.SLUG = this.slug = data.formInstituicao.data.SLUG;
          this.instituicao.CONTEUDO = data.formInstituicao.data.CONTEUDO;
          this.alertService.success('Todas as informações foram salvas!');
        },
        error => {
          this.alertService.error(
            'Ocorreu um erro ao tentar editar as informações da instituição!'
          );
          console.log(error);
        }
      );
  }

  resetForm() {
    console.warn('not implemented!');
  }
}
