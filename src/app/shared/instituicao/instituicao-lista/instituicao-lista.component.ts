import { Component, OnInit, Input } from '@angular/core';
import { NgForm } from '@angular/forms';

import { InstituicaoService } from '../../../services/instituicao.service';
import { InstituicaoListaModel } from '../../../models/instituicao';

@Component({
  selector: 'app-instituicao-lista',
  templateUrl: './instituicao-lista.component.html',
  styleUrls: ['./instituicao-lista.component.scss']
})
export class InstituicaoListaComponent implements OnInit {
  lastModified: InstituicaoListaModel[];

  constructor(private instituicaoService: InstituicaoService) {}

  ngOnInit() {
    this.getLastModified(3);
  }

  getLastModified(limit: number = 5) {
    this.instituicaoService
      .getall(null, limit)
      .subscribe(data => (this.lastModified = data));
  }
}
