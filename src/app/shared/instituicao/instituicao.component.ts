import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';

import { InstituicaoService } from '../../services/instituicao.service';


@Component({
  selector: 'app-instituicao',
  templateUrl: './instituicao.component.html',
  styleUrls: ['./instituicao.component.scss'],
})
export class InstituicaoComponent implements OnInit {

  constructor(
    public instituicaoService: InstituicaoService,
    private router: Router
  ) { }

  ngOnInit() { }

  onSelected(item) {
    if (item) {
      this.router.navigate([`instituicao/editar/${item.code}`]);
    }
  }

}
