import { Component, OnInit } from '@angular/core';

import { AuthService } from '../../services/auth/auth.service';
import { AlertService } from '../../services/alert/alert.service';
import { AlertModel } from '../../models/alert-model';
import { LoaderService } from '../../services/loader/loader.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public showAnimation: boolean = false;
  public showLoader: boolean = false;
  public alerts = Array<AlertModel>();

  constructor(
    private auth: AuthService,
    private alertService: AlertService,
    private loaderService: LoaderService,
  ) {
    alertService.alert$.subscribe((alert: AlertModel) => {
      this.alerts.push(alert);
      // Limitando a exibição de alertas para 3
      if (this.alerts.length === 4) {
        this.alerts.splice(0, 1);
      }

      if (alert.expires) {
        // Aumentando o tempo de exibição para mensagens com mais de 50 letras
        const timeout = alert.message.length > 50 ? 10000 : 5000;
        setTimeout(() => {
          this.alerts.splice(0, 1);
        }, timeout);
      }

    });

    loaderService.loader$.subscribe(
      (arg) => {
        this.showLoader = arg;
      }
    );
  }

  ngOnInit() {
    this.auth.startRenewTask();

    // @TODO colocar metodo da animação no tratamento das rotas
    setTimeout(() => {
      this.showAnimation = true;
    }, 500);
  }

  public closeAlert(alert) {
    const index: number = this.alerts.indexOf(alert);
    this.alerts.splice(index, 1);
  }
}
