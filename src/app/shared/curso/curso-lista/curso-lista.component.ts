import { Component, OnInit, Input } from '@angular/core';
import { NgForm } from '@angular/forms';

import { CursoService } from '../../../services/curso.service';
import { CursoListaModel } from '../../../models/curso';

@Component({
  selector: 'app-curso-lista',
  templateUrl: './curso-lista.component.html',
  styleUrls: ['./curso-lista.component.scss']
})
export class CursoListaComponent implements OnInit {
  lastModified: CursoListaModel[];

  constructor(private cursoService: CursoService) {}

  ngOnInit() {
    this.getLastModified(3);
  }

  getLastModified(limit: number = 5) {
    this.cursoService
      .getall(null, limit)
      .subscribe(data => (this.lastModified = data));
  }
}
