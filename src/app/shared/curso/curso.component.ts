import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CursoService } from '../../services/curso.service';

@Component({
  selector: 'app-curso',
  templateUrl: './curso.component.html',
  styleUrls: ['./curso.component.scss']
})
export class CursoComponent implements OnInit {

  constructor(
    public cursoService: CursoService,
    private router: Router
  ) { }

  ngOnInit() { }

  onSelected(item) {
    if (item) {
      this.router.navigate([`curso/editar/${item.code}`]);
    }
  }
}
