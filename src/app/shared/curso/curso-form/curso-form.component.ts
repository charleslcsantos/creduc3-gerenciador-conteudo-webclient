import { AlertService } from './../../../services/alert/alert.service';
import { ArquivoModel } from './../../../models/imagem';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FileUploader, FileItem } from 'ng2-file-upload';

import { CursoService } from '../../../services/curso.service';
import {
  CursoModel,
} from './../../../models/curso';
import { TamanhoImagem, ImagemModel } from '../../../models/imagem';
import { SeoModel } from '../../../models/seo';
import { LoaderService } from '../../../services/loader/loader.service';

import 'rxjs/add/operator/finally';
import { FileSaverService } from '../../../services/filesaver.service';

@Component({
  selector: 'app-curso-form',
  templateUrl: './curso-form.component.html',
  styleUrls: ['./curso-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CursoFormComponent implements OnInit {
  curso: CursoModel = {};
  seo: SeoModel = {};
  conteudo: string = '';
  apelido: string = '';
  slug: string = '';

  enabledFileSaver: Boolean = false;

  uploaderImagemCurso: FileUploader;

  editorOptions = {};

  selectedTab = 'conteudo';

  constructor(
    private cursoService: CursoService,
    private fileSaver: FileSaverService,
    private route: ActivatedRoute,
    private alertService: AlertService,
    private loaderService: LoaderService
  ) {
    this.loaderService.showLoader();
    this.editorOptions = {
      toolbar: {
        buttons: [
          'h2',
          'h3',
          'h4',
          'h5',
          'bold',
          'italic',
          'justifyFull',
          'anchor',
          'image',
          'orderedlist'
        ]
      },
      anchor: {
        placeholderText: 'Cole ou digite a url',
        targetCheckbox: true,
        targetCheckboxText: 'Abrir em nova janela'
      }
    };
  }

  ngOnInit() {
    this.route.params.subscribe(params => this.getCurso(params.id));

    this.onAfterAddingFile(this.uploaderImagemCurso);

    this.enabledFileSaver = this.fileSaver.browserSupports();
  }

  getCurso(id) {
    this.curso = {};
    this.uploaderImagemCurso = new FileUploader({ url: 'https://url-da-api' });

    this.cursoService.get(id).subscribe(data => {
      this.curso = data;
      this.slug = this.curso.SLUG;
      this.seo = this.curso.SEO;
      this.apelido = this.curso.NOME;

      try {
        this.conteudo = this.curso.CONTEUDO.TEXTO || '';
      } catch (error) {
        this.conteudo = '';
      }

      this.getImage(this.curso.IMAGENS.DESTAQUE);
      this.loaderService.showLoader(false);
    });
  }

  onAfterAddingFile(u: FileUploader) {
    u.onAfterAddingFile = f => {
      if (u.queue.length > 1) {
        u.removeFromQueue(u.queue[0]);
      }
    };
  }

  hasImageUploader(img): boolean {
    return (
      img &&
      'queue' in img &&
      img.queue &&
      img.queue.length > 0 &&
      img.queue[0]._file
    );
  }

  getImageUploader(img) {
    if (
      img &&
      'queue' in img &&
      img.queue &&
      img.queue.length > 0 &&
      img.queue[0]._file
    ) {
      return img.queue[0]._file;
    }
    return '';
  }

  getImage(destino: ImagemModel) {
    if (!destino) {
      return;
    }

    if (!('ARQUIVO' in destino) || destino.ARQUIVO == null) {
      destino.ARQUIVO = {};
    }

    destino.ARQUIVO.DADOS = '/assets/images/image-wireframe.png';
    this.cursoService
      .getImagem(destino.ARQUIVO.ID_ARQUIVO)
      .subscribe(arquivo => {
        if (arquivo) {
          destino.ARQUIVO.DADOS = arquivo;
        }
      });
  }

  hasSavedImage(key: string): boolean {

    key = key.toUpperCase();

    try {
      return Boolean(this.curso.IMAGENS[key].ARQUIVO.DADOS);
    } catch (error) {
      console.dir(error);
      return false;
    }
  }

  renderImage(key: string): string {
    key = key.toUpperCase();

    try {
      if (this.curso.IMAGENS[key].ARQUIVO.DADOS) {
        return this.curso.IMAGENS[key].ARQUIVO.DADOS;
      }
    } catch (err) {
      return '/assets/images/image-wireframe.png';
    }
  }

  publish(): void {
    this.cursoService
    .publish(this.curso.ID_DETALHE_CURSO.toString())
    .finally(() => this.loaderService.showLoader(false))
    .subscribe(
      data => {
        this.alertService.success('O site foi publicado');
        this.curso.CONTEUDO.FL_PUBLICAR = true;
      },
      error => {
        this.alertService.error(
          'Ocorreu um erro ao tentar publicar o site'
        );
      }
    );
  }

  downloadDoc(): void {
    this.fileSaver.downloadFile(this.curso.SLUG, this.curso.CONTEUDO.TEXTO, this.curso.CONTEUDO.DT_ATUALIZACAO_CONTEUDO);
  }

  saveDetail(): void {
    this.loaderService.showLoader();

    const model: CursoModel = {};
    model.ID_DETALHE_CURSO = this.curso.ID_DETALHE_CURSO;
    model.SLUG = this.slug;
    model.NOME = this.apelido;
    model.CONTEUDO = this.curso.CONTEUDO;

    this.cursoService
      .save(this.curso.ID_DETALHE_CURSO.toString(), model)
      .finally(() => this.loaderService.showLoader(false))
      .subscribe(
        data => {
          this.curso.SLUG = data.SLUG;
          this.slug = data.SLUG;
          this.alertService.success('As alterações foram salvas!');
        },
        error => {
          this.alertService.error(
            'Ocorreu um erro ao tentar editar os dados do curso'
          );
        }
      );
  }

  saveConteudo(): void {
    this.loaderService.showLoader();
    const cursoModel: CursoModel = {};
    cursoModel.ID_DETALHE_CURSO = this.curso.ID_DETALHE_CURSO;
    cursoModel.CONTEUDO = this.curso.CONTEUDO;
    cursoModel.CONTEUDO.TEXTO = this.conteudo;

    this.cursoService
      .save(this.curso.ID_DETALHE_CURSO.toString(), cursoModel)
      .finally(() => this.loaderService.showLoader(false))
      .subscribe(
        data => {
          this.alertService.success('O Conteúdo foi salvo!');
          this.curso.CONTEUDO = data.CONTEUDO;
        },
        error => {
          console.warn(error);
          this.alertService.error(
            'Ops. Ocorreu um erro ao tentar salvar o conteúdo!'
          );
        }
      );
  }

  saveImages(): void {
    this.loaderService.showLoader();
    const imagemCurso: FileItem = this.uploaderImagemCurso.queue[0];

    const form = new FormData();

    if (imagemCurso) {
      form.append(
        'imagem_curso',
        imagemCurso._file,
        imagemCurso.file.name
      );
    }

    this.cursoService
      .saveImages(this.curso.ID_DETALHE_CURSO.toString(), form)
      .finally(() => this.loaderService.showLoader(false))
      .subscribe(
        data => {
          this.alertService.success('A imagem foi atualizada!');
        },
        error => {
          this.alertService.error(
            'Ops. Ocorreu um erro ao tentar atualizar as imagens.'
          );
        }
      );
  }

  saveSeo(): void {
    this.loaderService.showLoader();
    let seoModel: SeoModel = {};
    seoModel = this.seo;

    this.cursoService
      .saveSeo(this.curso.CONTEUDO.ID_CONTEUDO, seoModel)
      .finally(() => this.loaderService.showLoader(false))
      .subscribe(
        data => {
          this.seo = data;
          this.alertService.success('As configurações de SEO foram salvas!');
        },
        error => {
          this.alertService.error(
            'Ops. Ocorreu um erro ao tentar salvar o conteúdo de SEO!'
          );
        }
      );
  }

  resetForm() {
    console.warn('Does not implemented!');
  }
}
