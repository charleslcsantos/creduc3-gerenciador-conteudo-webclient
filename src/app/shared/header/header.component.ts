import { Component, OnInit } from '@angular/core';

import { AuthService } from './../../services/auth/auth.service';
import { UsuarioModel } from '../../models/usuario';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  currentUser: UsuarioModel = {};

  constructor(private auth: AuthService) {}

  ngOnInit() {
    this.auth.loadUserData().subscribe(user => {
      this.currentUser = user;
      this.currentUser.PRIMEIRO_NOME = this.currentUser.NOME_COMPLETO.split(' ')[0];
    });
  }

  logout(e) {
    e.preventDefault();
    this.auth.logout();
  }
}
