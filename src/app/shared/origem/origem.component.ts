import { Component, OnInit, Input } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Component({
  selector: 'app-origem',
  templateUrl: './origem.component.html',
  styleUrls: ['./origem.component.scss']
})
export class OrigemComponent implements OnInit {

  private _origem = new BehaviorSubject<string>('');

  @Input() set origem(value: string) {
    this._origem.next(value);
  }

  get origem() {
    return this.parseOrigem(this._origem.getValue());
  }

  constructor() { }

  ngOnInit() { }

  parseOrigem(origem) {
    switch (origem) {
      case 'B':
        return 'Bolsa';
      case 'C':
        return 'CREDUC 2';
      case 'T':
        return 'CREDUC 3';
      default:
        return 'Não identificado';
    }
  }

}
