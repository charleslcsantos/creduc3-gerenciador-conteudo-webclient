export interface SearchModel {
  title: string;
  subtitle?: string;
  imagemPath?: string;
  slug: string;
  code: string;
  active?: boolean;
}

export interface ISearchService {
  search(term: any, limit?: number): any;
}
