import {
  Component,
  OnInit,
  Output,
  Input,
  forwardRef,
  EventEmitter,
  ViewChild,
  ElementRef,
  HostListener
} from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subscription } from 'rxjs/Subscription';

import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/switchMap';

import { of } from 'rxjs/observable/of';
import { _do } from 'rxjs/operator/do';
import { letProto } from 'rxjs/operator/let';
import { switchMap } from 'rxjs/operator/switchMap';
import { fromEvent } from 'rxjs/observable/fromEvent';

import { ISearchService, SearchModel } from './search.interface';

const noop = () => {};

const AUTOCOMPLETE_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => SearchComponent),
  multi: true
};

enum KEY {
  TAB = 9,
  ENTER = 13,
  ALTERNATIVE_ENTER = 10,
  ESCAPE = 27,
  ARROW_UP = 38,
  ARROW_DOWN = 40,
  BACKSPACE = 8
}

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  providers: [AUTOCOMPLETE_CONTROL_VALUE_ACCESSOR]
})
export class SearchComponent implements ControlValueAccessor, OnInit {
  @Input() service: ISearchService;
  @Input() list: any[] = [];
  @Input() placeholder: string = '';
  @Input() searchProperty: string = 'title';
  @Input() displayProperty: string = 'title';
  @Input() maxSuggestions: number = 5;

  @Output() suggestionSelected: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('inputElement') inputElement: ElementRef;

  public input: string = '';
  public typeahead: string;
  private previousInput: string;
  public suggestions: any[] = [];
  public areSuggestionsVisible: boolean = false;
  private selectedSuggestion: any;
  private activeSuggestion: SearchModel;
  private isDisabled: boolean = false;
  public searching: boolean = false;

  private _onTouched: () => void = noop;
  private _onChange: (_: any) => void = noop;

  private _subscription: Subscription;
  private _inputValueBackup: string;
  private _valueChanges: Observable<string>;
  private _resubscribeTypeahead: BehaviorSubject<any>;
  private _zoneSubscription: any;

  constructor(private _rootElementRef: ElementRef) {}

  ngOnInit() {
    this._valueChanges = fromEvent(this.inputElement.nativeElement, 'keyup', $e => $e.target.value);
    this._resubscribeTypeahead = new BehaviorSubject(null);

    const inputValues$ = _do.call(this._valueChanges, value => {
      this._inputValueBackup = value;
      this._onChange(value);
    });

    const results$ = letProto.call(inputValues$, (text$: Observable<string>) => {
      return text$
        .debounceTime(500)
        .distinctUntilChanged()
        .do(() => this.showSuggestions())
        .do(() => this.searching = true)
        .switchMap(term =>
          this.service
            .search(term, this.maxSuggestions)
            .map(items => this.list = items)
            .map(items => items.filter(item => item[this.searchProperty].toLowerCase().indexOf(term.toLowerCase()) > -1))
            .map(items => this.suggestions = items)
            .catch(error => {
              this.searching = false;
              return of([]);
            })
        )
        .do(() => this.searching = false);
    });

    const processedResults$ = _do.call(results$, () => {
      // this.populateSuggestions();
    });

    const userInput$ = switchMap.call(this._resubscribeTypeahead, () => processedResults$);
    this._subscription = this._subscribeToUserInput(userInput$);
  }

  @HostListener('document:click', ['$event'])
  handleClick(event) {
    if (event.target !== this.inputElement.nativeElement) {
      this.handleBlur();
      return;
    }
  }


  private _subscribeToUserInput(userInput$: Observable<any[]>): Subscription {
    return userInput$.subscribe((results) => this.populateSuggestions(results));
  }

  get value(): any {
    return this.selectedSuggestion;
  }

  set value(v: any) {
    if (v !== this.selectedSuggestion) {
      this.selectSuggestion(v);
      this._onChange(v);
    }
  }

  writeValue(value: any): void {
    if (value !== this.selectedSuggestion) {
      this.selectSuggestion(value);
    }
  }
  registerOnChange(fn: any): void {
    this._onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }

  setActiveSuggestion(suggestion: any) {
    this.activeSuggestion = suggestion;

    if (this.activeSuggestion) {
      this.activeSuggestion.active = true;
    }

    this.populateTypeahead();
  }

  getActiveSuggestionIndex() {
    let activeSuggestionIndex = -1;
    if (this.activeSuggestion != null) {
      activeSuggestionIndex = this.indexOfObject(
        this.suggestions,
        this.searchProperty,
        this.activeSuggestion[this.searchProperty]
      );
    }
    return activeSuggestionIndex;
  }

  handleKeyDown(event: KeyboardEvent) {
    const keycode = this.parseKeyCode(event);

    if (!KEY[keycode.toString()]) {
      return;
    }

    const activeSuggestionIndex = this.getActiveSuggestionIndex();

    switch (keycode) {
      case KEY.TAB:
        if (!this.areSuggestionsVisible) {
          return;
        }

        this.selectSuggestion(this.activeSuggestion);
        this.suggestions.splice(1);
        this.hideSuggestions();
        event.preventDefault();
        break;
      case KEY.ARROW_UP:
        if (activeSuggestionIndex === -1) {
          this.setActiveSuggestion(this.suggestions[0]);
          return;
        }

        if (activeSuggestionIndex === 0) {
          this.setActiveSuggestion(
            this.suggestions[this.suggestions.length - 1]
          );
        } else {
          this.setActiveSuggestion(this.suggestions[activeSuggestionIndex - 1]);
        }
        break;
      case KEY.ARROW_DOWN:
        if (activeSuggestionIndex === -1) {
          this.setActiveSuggestion(this.suggestions[0]);
          return 0;
        }

        if (activeSuggestionIndex === this.suggestions.length - 1) {
          this.setActiveSuggestion(this.suggestions[0]);
        } else {
          this.setActiveSuggestion(this.suggestions[activeSuggestionIndex + 1]);
        }
        break;
      case KEY.ENTER:
      case KEY.ALTERNATIVE_ENTER:
        if (this.areSuggestionsVisible) {
          this.selectSuggestion(this.activeSuggestion);
          this.hideSuggestions();
        }
        event.preventDefault();
        break;
      case KEY.ESCAPE:
        this.blurInputElement();
        if (this.areSuggestionsVisible) {
          this.suggestions = [];
          this.hideSuggestions();
        }
        break;
    }
  }

  handleFocus(event: FocusEvent) {
    if (this.selectedSuggestion != null) {
      this.input = '';
      this.selectSuggestion(null);
      this.populateTypeahead();
    }

    this.populateSuggestions();

    if (this.suggestions.length > 0) {
      this.populateTypeahead();
      this.showSuggestions();
    }
  }

  handleBlur(event?: Event) {
    this.typeahead = '';
    this.hideSuggestions();
    this._onTouched();
  }

  populateSuggestions(suggestions?) {
    if (!suggestions) {
      suggestions = this.suggestions;
    }

    if (!suggestions || suggestions.length === 0) {
      this.typeahead = '';
    } else {
      this.populateTypeahead();
      this.setActiveSuggestion(this.suggestions[0]);
      if (!this.areSuggestionsVisible) {
        this.showSuggestions();
      }
    }
  }

  populateTypeahead() {
    if (this.activeSuggestion == null || !this.areSuggestionsVisible) {
      this.typeahead = '';
      return;
    }

    this.typeahead =
      this.input +
      (this.activeSuggestion[this.displayProperty] || '').slice(
        this.input.length
      );
  }

  selectSuggestion(suggestion: any): any {
    this.selectedSuggestion = suggestion;
    this.suggestionSelected.emit(suggestion);
    this.hideSuggestions();

    if (this.selectedSuggestion != null) {
      this.input = suggestion[this.displayProperty];
      this.typeahead = suggestion[this.displayProperty];
      this.blurInputElement();
    }
  }

  suggestionMouseOver(suggestion: any) {
    console.log('mouse over');
    this.setActiveSuggestion(suggestion);
  }

  suggestionMouseDown(suggestion: any) {
    this.selectSuggestion(suggestion);
  }

  suggestionsMouseOut(event: MouseEvent) {
    this.setActiveSuggestion(null);
  }

  hideSuggestions(): void {
    this.toggleSuggestion(false);
  }

  showSuggestions(): void {
    this.toggleSuggestion(true);
  }

  toggleSuggestion(bool: boolean): void {
    this.areSuggestionsVisible = bool;
  }

  hasSelection(): boolean {
    return this.selectedSuggestion != null;
  }

  hasItems(): boolean {
    return this.list.length > 0;
  }

  blurInputElement() {
    if (this.inputElement && this.inputElement.nativeElement) {
      this.inputElement.nativeElement.blur();
    }
  }

  parseKeyCode(event: KeyboardEvent): number {
    return event.which || event.keyCode;
  }

  indexOfObject(array: any[], property: string, value: string) {
    if (array == null || array.length === 0) {
      return -1;
    }

    let index = -1;
    for (let i = 0; i < array.length; i++) {
      if (array[i][property] != null && array[i][property] === value) {
        index = i;
      }
    }
    return index;
  }

  itemIsActive(item): boolean {
    return (
      item &&
      this.activeSuggestion &&
      this.activeSuggestion.active &&
      item.code === this.activeSuggestion.code
    );
  }
}
