import { Injectable } from '@angular/core';
import {
  Http,
  XHRBackend,
  Response,
  Headers,
  Request,
  RequestOptions,
  RequestOptionsArgs,
  ConnectionBackend
} from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

export interface ApiHttpSettings {
  baseUrl: string;
  retrieveToken: Function;
  catchErrors?: Function;
}

export interface ApiHttpToken {
  access_token: string;
  refresh_token: string;
  expires_in?: number;
  token_type?: string;
}

@Injectable()
export class ApiHttpProvider extends Http {
  private secureApi: ApiHttpProvider;
  private settings: ApiHttpSettings;
  private token: ApiHttpToken;
  private isSecure = false;
  private alertConnection = [];

  constructor(
    private backend: ConnectionBackend,
    private defaultOptions: RequestOptions
  ) {
    super(backend, defaultOptions);
  }

  setAsSecure(): void {
    this.isSecure = true;
  }

  configure(settings: ApiHttpSettings): void {
    this.settings = settings;
  }

  secure(): ApiHttpProvider {
    if (!this.secureApi) {
      this.secureApi = new ApiHttpProvider(this.backend, this.defaultOptions);
      this.secureApi.configure(this.settings);
      this.secureApi.setAsSecure();
    }
    return this.secureApi;
  }

  makeurl(endpoint: any) {
    const protocolPattern = /(http(s?))\:\/\//gi;

    if (protocolPattern.test(endpoint)) {
      return endpoint;
    }

    return `${this.settings.baseUrl}${endpoint}`;
  }

  private doRequest(
    url: string | Request,
    options?: RequestOptionsArgs
  ): Observable<Response> {
    if (typeof url === 'string') {
      url = this.makeurl(url);
    } else {
      url.url = this.makeurl(url.url);
    }

    if (this.isSecure) {
      if (typeof url === 'string') {
        if (!options) {
          options = { headers: new Headers() };
        }
        options.headers.set(
          'Authorization',
          `${this.token.token_type} ${this.token.access_token}`
        );
      } else {
        url.headers.set(
          'Authorization',
          `${this.token.token_type} ${this.token.access_token}`
        );
      }
    }

    return super.request(url, options)
      .map((res: Response | any) => {
        if (res && res.status) {
          this.alertConnection.splice(0, 1);
        }
        return res;
      })
      .catch((error: Response | any) => {
        try {
          this.settings.catchErrors(error, this.isSecure, this.alertConnection);
        } catch (err) {}

        return error;
      });
  }

  request(
    url: string | Request,
    options?: RequestOptionsArgs
  ): Observable<Response> {
    if (this.isSecure) {
      this.token = this.settings.retrieveToken();
    }
    return this.doRequest(url, options);
  }
}
