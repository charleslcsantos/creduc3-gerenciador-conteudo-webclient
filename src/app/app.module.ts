import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { Http, XHRBackend, RequestOptions, ConnectionBackend, HttpModule } from '@angular/http';

import { MediumEditorDirective } from '../../node_modules/angular2-medium-editor/medium-editor.directive';
// import { FileSelectDirective } from 'ng2-file-upload';
import { FileUploadModule } from 'ng2-file-upload/file-upload/file-upload.module';
import { ImagePreviewDirective } from './provider/image-upload-preview/image-upload-preview.directive';

import { appRoutes } from './app.routes';

import { AppComponent } from './app.component';

import { MomentModule } from 'angular2-moment';
import * as moment from 'moment';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';

import { HomeComponent } from './shared/home/home.component';
import { HeaderComponent } from './shared/header/header.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { OrigemComponent } from './shared/origem/origem.component';


import { httpFactory } from './app.configure';
import { ApiHttpProvider, ApiHttpSettings } from './provider/http/apihttp.provider';

import {
  AuthService,
  AuthGuardService,
  AuthGuardIsLogged
} from './services/auth/';

import { StorageService } from './services/storage/storage.service';
import { InstituicaoService } from './services/instituicao.service';


import {
  InstituicaoComponent,
  InstituicaoFormComponent,
  InstituicaoListaComponent
} from './shared/instituicao/';

import {
  SearchComponent
} from './shared/search/';
import { Router } from '@angular/router';
import { AlertService } from './services/alert/alert.service';
import { LoaderService } from './services/loader/loader.service';

import {
  CursoComponent,
  CursoFormComponent,
  CursoListaComponent
} from './shared/curso';
import { CursoService } from './services/curso.service';
import { FileSaverService } from './services/filesaver.service';
import { UtilService } from './services/utils/utils.service';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NotFoundComponent,
    HeaderComponent,
    HomeComponent,
    SidebarComponent,
    SearchComponent,
    OrigemComponent,
    InstituicaoComponent,
    InstituicaoFormComponent,
    InstituicaoListaComponent,
    CursoComponent,
    CursoFormComponent,
    CursoListaComponent,
    MediumEditorDirective,
    ImagePreviewDirective,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot(appRoutes, { enableTracing: false }),
    FileUploadModule,
    MomentModule,
    NgbModule.forRoot(),
    // SharedModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    ApiHttpProvider,
    AuthService,
    AuthGuardService,
    AuthGuardIsLogged,
    AlertService,
    LoaderService,
    StorageService,
    InstituicaoService,
    CursoService,
    UtilService,
    FileSaverService,
    {
      provide: ApiHttpProvider,
      useFactory: httpFactory,
      deps: [XHRBackend, RequestOptions, Router, AlertService]
    },
    {
      provide: ConnectionBackend,
      useClass: XHRBackend
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor() {
    moment.locale('pt-br');
  }

}
