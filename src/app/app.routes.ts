import { Routes } from '@angular/router';

// Service
import { AuthGuardService as AuthGuard, AuthGuardIsLogged } from './services/auth/auth-guard.service';

// Componentes
import { HomeComponent } from './shared/home/home.component';

import {
  InstituicaoComponent,
  InstituicaoFormComponent,
  InstituicaoListaComponent
} from './shared/instituicao/';

import {
  CursoComponent
} from './shared/curso';

import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { CursoListaComponent } from './shared/curso/curso-lista/curso-lista.component';
import { CursoFormComponent } from './shared/curso/curso-form/curso-form.component';


export const appRoutes: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthGuard], canActivateChild: [AuthGuard],
    children: [
      { path: 'instituicao', component: InstituicaoComponent,
        children: [
          { path: '', component: InstituicaoListaComponent },
          { path: 'editar/:id', component: InstituicaoFormComponent }
        ]
      },
      { path: 'curso', component: CursoComponent,
        children: [
          { path: '', component: CursoListaComponent },
          { path: 'editar/:id', component: CursoFormComponent }
        ]
      }
    ]
  },
  { path: 'login', component: LoginComponent, canActivate: [AuthGuardIsLogged] },
  { path: '**',    component: NotFoundComponent }
];
