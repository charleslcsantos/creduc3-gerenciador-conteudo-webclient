import { Router } from '@angular/router';
import { XHRBackend, RequestOptions, Http } from '@angular/http';

import { environment } from '../environments/environment';

import {
  ApiHttpProvider,
  ApiHttpSettings
} from './provider/http/apihttp.provider';
import { StorageService } from './services/storage/storage.service';
import { AuthService } from './services/auth/auth.service';
import { AlertService } from './services/alert/alert.service';

export function httpFactory(
  xhrBackend: XHRBackend,
  requestOptions: RequestOptions,
  router: Router,
  alertService: AlertService
): Http {
  const config: ApiHttpSettings = {
    baseUrl: environment.baseUrl,
    retrieveToken: () => {
      return StorageService.get('token');
    },
    catchErrors: (error, isSecure, alertConnection) => {
      if (!error.status && alertConnection.length === 0) {
        alertConnection.push(true);
        alertService.warning('Há um problema de comunicação com o servidor!', false);
      }
      if (isSecure && (error.status === 401 || error.status === 403)) {
        StorageService.clear('token');
        router.navigate(['/login'], {
          queryParams: {
            redirecTo: router.url
          }
        });
      }
    }
  };

  const api = new ApiHttpProvider(xhrBackend, requestOptions);

  api.configure(config);
  return api;
}
