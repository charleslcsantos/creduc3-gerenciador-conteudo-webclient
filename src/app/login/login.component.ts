import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AuthService } from './../services/auth/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;
  message: string;
  redirectTo: string;

  public showAnimation: boolean = false;
  showLoading: boolean;

  constructor(
    private auth: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.queryParams
      .subscribe(params => this.redirectTo = params.redirectTo || '/instituicao');

    // @TODO colocar metodo da animação no tratamento das rotas
    setTimeout(() => {
      this.showAnimation = true;
    }, 500);
  }

  submit(e) {
    e.preventDefault();
    this.showLoading = true;
    this.auth
      .login(this.username.toLowerCase(), this.password)
      .subscribe(
        success => {
          this.showLoading = false;
          this.router.navigateByUrl(this.redirectTo);
        },
        error => {
          this.showLoading = false;
          this.message = 'Hmm..esse login e senha estão incorretos.';
          setTimeout(() => {
            this.message = '';
          }, 7000);
        }
      );
  }

}
