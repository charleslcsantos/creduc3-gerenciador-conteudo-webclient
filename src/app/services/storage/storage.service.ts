import { Injectable } from '@angular/core';

@Injectable()
export class StorageService {
  static store(key: string, value: any): void {
    localStorage.setItem(key, JSON.stringify(value));
  }

  static get(key: string): any {
    return JSON.parse(localStorage.getItem(key));
  }

  static has(key: string): boolean {
    return Boolean(this.get(key));
  }

  static clear(key?: string): void {
    if (key) {
      localStorage.removeItem(key);
    } else {
      localStorage.clear();
    }
  }
}
