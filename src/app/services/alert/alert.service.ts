import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { AlertModel } from '../../models/alert-model';

@Injectable()
export class AlertService {
  private alertSource = new Subject<object>();
  public alert$ = this.alertSource.asObservable();

  constructor() {}

  /**
   * Cria um alerta na tela
   * alert
   * @param {string} m : mensagem a ser exibida
   * @param {string} t: tipo de mensagem [success, warning, danger, info]
   * @param {boolean} e: se o alerta deve expirar
   */
  public alert(message: string, type: string, expires: boolean = true): void {
    const newAlert: AlertModel = {
      message: message,
      type: type,
      expires: expires
    };
    this.alertSource.next(newAlert);
  }

  /**
   * Cria um alerta do tipo "success" na tela
   * alert
   * @param {string} m : mensagem a ser exibida
   */
  public success(m: string): void {
    this.alert(m, 'success');
  }

  /**
   * Cria um alerta do tipo "warning" na tela
   * alert
   * @param {string} m : mensagem a ser exibida
   * @param {boolean} e: se o alerta deve expirar
   */
  public warning(m: string, e: boolean = true): void {
    this.alert(m, 'warning', e);
  }

  /**
   * Cria um alerta do tipo "error" na tela
   * alert
   * @param {string} m : mensagem a ser exibida
   */
  public error(m: string): void {
    this.alert(m, 'danger');
  }

  /**
   * Cria um alerta do tipo "info" na tela
   * alert
   * @param {string} m : mensagem a ser exibida
   */
  public info(m: string): void {
    this.alert(m, 'info');
  }
}
