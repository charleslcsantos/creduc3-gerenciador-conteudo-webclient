import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ApiHttpProvider } from '../../provider/http/apihttp.provider';

@Injectable()
export class UtilService {

    private endpoint = '/utils/slugify';

    constructor(private http: ApiHttpProvider){}

    slugify(slug: string): Observable<string> {
        console.log(slug);
        slug = slug.replace(/[^\w\s-]/gi, '');
        console.log(slug);
        return Observable.create(observer => {
          this.http
            .secure()
            .get(`${this.endpoint}/${slug}`)
            .map(res => res.json())
            .subscribe(
              data => {
                observer.next(data.DADOS);
                observer.complete();
              },
              error => {
                observer.error(error);
                observer.complete();
              }
            );
        });
      }
}