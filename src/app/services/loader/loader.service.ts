import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class LoaderService {
  private loaderSource = new Subject<boolean>();
  public loader$ = this.loaderSource.asObservable();

  constructor() { }

  public showLoader(arg: boolean = true) {
    this.loaderSource.next(arg);
  }

}
