import { StorageService } from './../storage/storage.service';
import { Injectable } from '@angular/core';
import {
  Router,
  CanActivate,
  CanActivateChild,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';

import { AuthService } from './../auth/auth.service';
import { LoginComponent } from './../../login/login.component';


@Injectable()
export class AuthGuardService implements CanActivate, CanActivateChild {

  constructor(
    private auth: AuthService,
    private router: Router
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.auth.isAuthenticated()) {
      return true;
    }

    if (StorageService.has('token')) {
      StorageService.clear('token');
    }

    this.router.navigate(['/login'], {
      queryParams: {
        redirectTo: state.url
      }
    });
    return false;
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.canActivate(route, state);
  }
}

@Injectable()
export class AuthGuardIsLogged implements CanActivate {

  constructor(
    private auth: AuthService,
    private router: Router
  ) { }

  canActivate(): boolean {
    if (!this.auth.isAuthenticated()) {
      return true;
    }

    this.router.navigate(['/instituicao']);
    return false;
  }
}
