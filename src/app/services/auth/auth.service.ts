import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { URLSearchParams } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { ISubscription } from 'rxjs/Subscription';
import { IntervalObservable } from 'rxjs/observable/IntervalObservable';
import 'rxjs/add/operator/takeWhile';

import { ApiHttpProvider } from './../../provider/http/apihttp.provider';
import { StorageService } from '../storage/storage.service';
import { UsuarioModel } from '../../models/usuario';

@Injectable()
export class AuthService {

  private endpoint = '/GerarToken';
  private endpointUsuario = '/usuario';
  private timeout: number = 1000 * 60 * 30;
  private timer: Observable<number>;
  private $timer: ISubscription;
  private currentUser: UsuarioModel = {};

  constructor(private http: ApiHttpProvider, private router: Router) {}

  login(username: string, password: string): Observable<boolean> {
    const params: URLSearchParams = new URLSearchParams();
    params.set('grant_type', 'password');
    params.set('username', username);
    params.set('password', password);
    params.set('args', '');

    return Observable.create(observer => {
      this.http
        .post(this.endpoint, params.toString())
        .map(res => res.json())
        .subscribe(
          token => {
            StorageService.store('token', token);
            observer.next(true);
            observer.complete();
          },
          error => {
            observer.error(false);
            observer.complete();
          }
        );
    });
  }

  loadUserData(): Observable<UsuarioModel> {

    return Observable.create((observer) => {

      if (this.currentUser.ID_USUARIO > 0) {
        observer.next(this.currentUser);
        observer.complete();
        return;
      }

      this.http
        .secure()
        .get(this.endpointUsuario)
        .map(res => res.json())
        .subscribe((data) => {
          this.currentUser = <UsuarioModel>data.DADOS;
          observer.next(this.currentUser);
          observer.complete();
        }, error => {
          observer.error(error);
          observer.complete();
        });

      });
  }

  logout(): void {

    this.http
      .secure()
      .get(`${this.endpointUsuario}/logout`)
      .subscribe(() => {
        StorageService.clear('token');
        this.router.navigate(['/login']);
      });

      // TODO: Tratar o erro
  }

  isAuthenticated(): boolean {
    return StorageService.has('token');
  }

  renew() {
    const token = StorageService.get('token');
    const params: URLSearchParams = new URLSearchParams();

    params.set('grant_type', 'refresh_token');
    params.set('refresh_token', token.refresh_token);

    return Observable.create(observer => {
      this.http
        .post(this.endpoint, params.toString())
        .map(res => res.json())
        .subscribe(
          newToken => {
            StorageService.store('token', newToken);
            observer.next(true);
            observer.complete();
          },
          error => {
            StorageService.clear('token');
            this.$timer.unsubscribe();
            this.router.navigate(['login']);
            observer.error(false);
            observer.complete();
          }
        );
    });
  }

  startRenewTask() {
    if (!this.timer) {
      this.timer = IntervalObservable.create(this.timeout).takeWhile(() =>
        this.isAuthenticated()
      );
    }

    this.$timer = this.timer.subscribe(() => {
      this.renew().subscribe();
    });
  }
}
