import { ArquivoModel } from './../models/imagem';
import { BrowserModule } from '@angular/platform-browser';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { ApiHttpProvider } from './../provider/http/apihttp.provider';
import { ISearchService, SearchModel } from './../shared/search';
import {
  CursoModel,
  CursoListaModel,
  CursoListaImagensModel,
} from './../models/curso';
import { SeoModel } from '../models/seo';
import * as Blob from 'blob';
import * as FileSaver from 'file-saver';
import { FileSaverService } from './filesaver.service';

@Injectable()
export class CursoService implements ISearchService {
  private endpoint = '/cursos';

  constructor(private http: ApiHttpProvider) {}

  get(id: string): Observable<CursoModel> {
    return Observable.create(observer => {
      this.http
        .secure()
        .get(`${this.endpoint}/${id}`)
        .map(res => res.json())
        .subscribe(
          data => {
            observer.next(<CursoModel>data.DADOS);
            observer.complete();
          },
          error => {
            observer.error(error);
            observer.complete();
          }
        );
    });
  }

  getall(query?: string, limit?: Number): Observable<CursoListaModel[]> {
    const params: URLSearchParams = new URLSearchParams();

    if (query) {
      params.set('busca', query);
    }

    if (limit) {
      params.set('limite', limit.toString());
    }

    return Observable.create(observer => {
      this.http
        .secure()
        .get(`${this.endpoint}?${params.toString()}`)
        .map(res => res.json())
        .subscribe(
          data => {
            observer.next(<CursoListaModel[]>data.DADOS);
            observer.complete();
          },
          error => {
            observer.next([]);
            observer.complete();
          }
        );
    });
  }

  getImagem(idArquivo: Number): Observable<string> {
    const imageDefault = '/assets/images/image-wireframe.png';

    return Observable.create( observer => {

      if (!idArquivo) {
        observer.next(imageDefault);
        observer.complete();
        return;
      }

      this.http
        .secure()
        .get(`/arquivos/${idArquivo}`)
        .map(res => res.json())
        .subscribe(
          data => {
            const arquivo = data.DADOS;
            const prefix = 'data:image/png';
            const bytestring = `${prefix};base64,${arquivo.DADOS}`;
            observer.next(bytestring);
            observer.complete();
          },
          error => {
            observer.next(imageDefault);
            observer.complete();
          });
      });
  }

  save(code: string, params: CursoModel): Observable<CursoModel> {

    return Observable.create(observer => {
      this.http
        .secure()
        .put(this.endpoint, params)
        .map(res => res.json())
        .subscribe(
          data => {
            observer.next(<CursoModel>data.DADOS);
            observer.complete();
          },
          error => {
            observer.error(error);
            observer.complete();
          }
        );
    });
  }

  saveSeo(idConteudo: Number, params: SeoModel): Observable<SeoModel> {
    return Observable.create(observer => {
      this.http
        .secure()
        .put(`/seo/${idConteudo}`, params)
        .map(res => res.json())
        .subscribe(
          data => {
            observer.next(<SeoModel>data.DADOS);
            observer.complete();
          },
          error => {
            observer.error(error);
            observer.complete();
          }
        );
    });
  }

  saveImages(code: string, params: any): Observable<CursoModel> {
    return Observable.create(observer => {
      this.http
        .secure()
        .put(`${this.endpoint}/${code}/imagens`, params)
        .map(res => res.json())
        .subscribe(
          data => {
            observer.next(<CursoListaImagensModel>data.DADOS);
            observer.complete();
          },
          error => {
            observer.error(error);
            observer.complete();
          }
        );
    });
  }

  saveAll(code: string, params: any): Observable<any> {
    const response = {
      formCurso: { error: false, data: null },
      formImages: { error: false, data: null }
    };
    return Observable.create( observer => {
      this.save(code, params.formCurso).subscribe(
        successSave => {
          response.formCurso.data = successSave;
          this.saveImages(code, params.formImagens).subscribe(
            successSaveImages => {
              response.formImages.data = successSaveImages;
              observer.next(response);
              observer.complete();
            },
            error => {
              response.formCurso.error = false;
              response.formImages.error = true;
              observer.error(error);
              observer.complete();
            }
          );

        },
        error => {
          response.formCurso.error = true;
          observer.error(error);
          observer.complete();
        }
      );
    });
  }

  publish(code: string): Observable<boolean> {
    return Observable.create(observer => {
      this.http
        .secure()
        .put(`${this.endpoint}/${code}/publicar`, null)
        .map(res => res.json())
        .subscribe(
          data => {
            observer.next(<boolean>data.DADOS);
            observer.complete();
          },
          error => {
            observer.error(false);
            observer.complete();
          }
        );
    });
  }

  search(term: string, limit?: number): Observable<SearchModel[]> {
    return Observable.create( observer => {

      if (!term || term.length < 3) {
        observer.error(new Error('The length of term is short'));
        observer.complete();
        return;
      }

      const items: SearchModel[] = [];

      this
        .getall(term, limit)
        .subscribe(
          cursos => {

            cursos.forEach(curso => {
              items.push({
                title: curso.NOME,
                slug: curso.SLUG,
                code: curso.ID_DETALHE_CURSO.toString(),
                active: false
              });
            });

            observer.next(items);
            observer.complete();
          },
          error => {
            observer.error(error);
            observer.complete();
          }
        );

    });
  }
}
