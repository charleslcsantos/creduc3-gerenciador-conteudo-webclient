import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import * as Blob from 'blob';
import * as FileSaver from 'file-saver';
import * as moment from 'moment';

const momentConstructor: (value?: any) => moment.Moment = (<any>moment).default || moment;

@Injectable()
export class FileSaverService {

  constructor() { }

  generateDateString(data: Date = null) {

    if (!data) {
      data = new Date();
    }

    return momentConstructor(data).format('YYYYMMDDhhmm');
  }

  browserSupports(): Boolean {
    try {
      return !!new Blob;
    } catch (e) {
      console.warn(e);
      return false;
    }
  }

  downloadFile(fileName: string, content: string, timeWhen: Date = null): Boolean {

    if (!this.browserSupports()) {
      return false;
    }

    fileName += this.generateDateString(timeWhen);

    try {
      const htmlDocument = `
      <!DOCTYPE html>
      <html>
      <head>
      <meta charset="utf-8">
      <title>${fileName}</title>
      </head>
      <body>
      ${content}
      </body>
      </html>`;

      const dataBlob = new Blob([htmlDocument], {
        type: 'text/html'
      });

      FileSaver.saveAs(dataBlob, `${fileName}.html`);
      return true;

    } catch (error) {

      console.warn(error);

      return false;
    }
  }

}
